import React, {useState} from 'react';
import Ingredients from "../components/Ingredients";
import BurgerVisual from "../components/BurgerVisual";
import meatImg from "../assets/meat.png";
import saladImg from "../assets/salad.png";
import cheeseImg from "../assets/cheese.jpg";
import baconImg from "../assets/bacon.png";
import './Burger.css';

const INGREDIENTS = [
    {name: 'Meat', price: 50, img: meatImg},
    {name: 'Salad', price: 5, img: saladImg},
    {name: 'Cheese', price: 20, img: cheeseImg},
    {name: 'Bacon', price: 30, img: baconImg},
];

const Burger = () => {
    const [ingredients, setIngredients] = useState([
        {name: 'Meat', count: 0},
        {name: 'Salad', count: 0},
        {name: 'Cheese', count: 0},
        {name: 'Bacon', count: 0},
    ]);

    const [price, setPrice] = useState(20);

    const getTotalPrice = () => {
        let total = 20;
        ingredients.forEach((item, idx) => {
          let counter = 0;
          while (counter !== item.count) {
              total += INGREDIENTS[idx].price;
              counter++;
          }
        })
        return total;
    };

    const addIngredient = name => {
        setIngredients(ingredients.map(item => {
            if (item.name === name) {
                return {
                    ...item,
                    count: item.count += 1
                };
            }
            return item;
        }));
        setPrice(getTotalPrice());
    };

    const removeOneIngredient = name => {
        setIngredients(ingredients.map(item => {
            if (item.name === name) {
                if (item.count !== 0) {
                    return {
                        ...item,
                        count: item.count -= 1
                    };
                }
            }
            return item;
        }));
        setPrice(getTotalPrice());
    }

    return (
        <div className="main-content">
            <Ingredients
                addIngredient={addIngredient}
                removeOneIngredient={removeOneIngredient}
                ingredients={INGREDIENTS}
                ingredientsState={ingredients}
            />
            <BurgerVisual
                price={price}
                ingredientsState={ingredients}
            />
        </div>
    );
};

export default Burger;