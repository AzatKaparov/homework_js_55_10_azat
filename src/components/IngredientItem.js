import React from 'react';

const IngredientItem = ({ icon, name, count, onImgClick, onRemove}) => {
    return (
        <div className="ingred-item">
            <img
                onClick={onImgClick}
                className="ingred-icon"
                src={icon}
                alt={name}
            />
            <p className="ingred-name">{name}</p>
            <p className="ingred-count">x{count}</p>
            <button onClick={onRemove} className="ingred-remove">Remove</button>
        </div>
    );
};

export default IngredientItem;