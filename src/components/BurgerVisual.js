import React from 'react';
import { v4 } from 'uuid';

const BurgerVisual = ({ ingredientsState, price }) => {
    const ingredientsVisual = [];
    ingredientsState.forEach(item => {
        let counter = 0;
        while (counter !== item.count) {
            ingredientsVisual.push(<div key={v4()} className={item.name} />);
            counter++;
        }
    })

    return (
        <div className="burger-block">
            <h3 className="burger-title">Burger</h3>
            <div className="Burger">
                <div className="BreadTop">
                    <div className="Seeds1"/>
                    <div className="Seeds2"/>
                </div>
                {ingredientsVisual}
                <div className="BreadBottom"/>
            </div>
            <h5>Price: {price} KGS</h5>
        </div>
    );
};

export default BurgerVisual;