import React from 'react';
import IngredientItem from "./IngredientItem";

const Ingredients = ({ ingredients, ingredientsState, addIngredient, removeOneIngredient }) => {
    return (
        <div className="ingred-block">
            <h3 className="ingred-title">Ingredients</h3>
            <div className="ingred-list">
                {ingredients.map((item, idx) => {
                    return (
                        <IngredientItem
                            onImgClick={() => addIngredient(item.name)}
                            key={item.name}
                            name={item.name}
                            icon={item.img}
                            count={ingredientsState[idx].count}
                            onRemove={() => removeOneIngredient(item.name)}
                        />
                    );
                })}
            </div>
        </div>
    );
};

export default Ingredients;