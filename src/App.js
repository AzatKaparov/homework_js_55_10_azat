import './App.css'
import Burger from "./containers/Burger";


function App() {
  return (
    <div className="container">
        <Burger/>
    </div>
  );
}

export default App;
